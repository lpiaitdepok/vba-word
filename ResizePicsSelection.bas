Sub ResizePicsSelection()
Dim shp As Word.Shape
Dim lWidth As Long
Dim lHeight As Long
Dim bUnit As Byte

If Word.Selection.Type <> wdSelectionInlineShape And _
Word.Selection.Type <> wdSelectionShape Then
Exit Sub
End If

Word.Selection.ShapeRange.LockAspectRatio = True

bUnit = InputBox("Unit Measurement, 1 for inch, 2 for cm, 3 for points")
lWidth = InputBox("Input width")
lHeight = InputBox("Input height")

Select Case bUnit
    Case 1
        lWidth = InchesToPoints(lWidth)
        lHeight = InchesToPoints(lHeight)
    Case 2
        lWidth = CentimetersToPoints(lWidth)
        lHeight = CentimetersToPoints(lHeight)
End Select

For Each sh In Word.Selection.ShapeRange
    sh.Width = lWidth
    sh.Height = lHeight
    sh.WrapFormat.Type = WdWrapType.wdWrapSquare
Next sh
End Sub