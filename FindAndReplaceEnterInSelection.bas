Sub FindAndReplaceEnterInSelection()
Dim strFindText As String
Dim strReplaceText As String

If Selection.Type = wdSelectionIP Then
    MsgBox "You don't select anything"
    Exit Sub
End If

  strFindText = InputBox("Enter finding text here:")
  strReplaceText = InputBox("Enter replacing text here:")

If strFindText = vbNullString Then
    strFindText = "^p"
End If
If strReplaceText = vbNullString Then
    strReplaceText = " "
End If


With Selection.Find
    .Text = strFindText
    .Replacement.Text = strReplaceText
    .Forward = True
    .Wrap = wdFindStop
End With

Selection.Find.Execute Replace:=wdReplaceAll
End Sub