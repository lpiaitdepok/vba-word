'This example adds bullets and formatting to the paragraphs in the selection. If there are already bullets in the selection, the example removes the bullets and formatting.
'the default constant is wdWord8ListBehavior, use wdWord9ListBehavior to take advantage of improved Web-oriented formatting with respect to indenting and multilevel lists.
Selection.Range.ListFormat.ApplyBulletDefault

referensi:
https://docs.microsoft.com/en-us/